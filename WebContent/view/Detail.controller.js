sap.ui.controller("vkc.view.Detail", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf view.Detail
*/
	onInit: function() {
		this.getView().byId("columnChartId").addStyleClass("Show");
		this.getView().byId("donutChartId").addStyleClass("Hide");
		this.getView().byId("lineChartId").addStyleClass("Hide");
		this.getView().byId("homePageId").addStyleClass("Hide");
		this.getView().byId("TablePageId").addStyleClass("Hide");
		 
		//IconTabBar hide 	
			sap.ui.getCore().byId("Detail--idIconTabBarMulti").addStyleClass("Hide");
			sap.ui.getCore().byId("Detail--idIconTabBarMulti").removeStyleClass("Show");
		
			sap.ui.getCore().byId("Detail--detailBtn1").addStyleClass("Hide");
			sap.ui.getCore().byId("Detail--detailBtn1").removeStyleClass("Show");
		/*this.byId("openMenu").attachBrowserEvent("tab keyup", function(oEvent){
		this._bKeyboard = oEvent.type == "keyup";
	}, this);*/
			var oVizFrame = this.getView().byId("TablePageId");
			var oModel = new sap.ui.model.json.JSONModel();
			var data = {
					empList : [{
		                "State": "Maharashtra",
		                "Revenue": 469378.12,
		                "Profit": 173793.31,
		                "Cost": 295584.81,
		                "Units Available": 17336,
		                "Unit Price": 21240.79,
		                "Units Sold": 57571
		            }, {
		                "State": "Tamil Nadu & Pondy",
		                "Revenue": 355940.33,
		                "Profit": 140874.87,
		                "Cost": 215065.45,
		                "Units Available": 11270,
		                "Unit Price": 13139.07,
		                "Units Sold": 48552
		            }, {
		                "State": "Karnataka",
		                "Revenue": 172126.37,
		                "Profit": 56994.34,
		                "Cost": 115132.04,
		                "Units Available": 11248,
		                "Unit Price": 79363.21,
		                "Units Sold": 37303
		            }, {
		                "State": "Kerala",
		                "Revenue": 252835.82,
		                "Profit": 81093.4,
		                "Cost": 171742.42,
		                "Units Available": 14917,
		                "Unit Price": 1143.57,
		                "Units Sold": 51664
		            }, {
		                "State": "Gujarat",
		                "Revenue": 481499.18,
		                "Profit": 150465.23,
		                "Cost": 331033.94,
		                "Units Available": 22449,
		                "Unit Price": 28268.02,
		                "Units Sold": 69005
		            }, {
		            	"State": "Andhra & Telangana",
		                "Revenue": 323097.02,
		                "Profit": 115242.56,
		                "Cost": 207854.46,
		                "Units Available": 17996,
		                "Unit Price": 14566.91,
		                "Units Sold": 45346
		            }, {
		                "State": "Rajasthan",
		                "Revenue": 368528.87,
		                "Profit": 124653.66,
		                "Cost": 243875.22,
		                "Units Available": 23662,
		                "Unit Price": 13469.83,
		                "Units Sold": 66737
		            }
			]};

			oModel.setData(data);
			oVizFrame.setModel(oModel,"ui5empModel");
		
///// Column Chart	
		//1.Get the id of the VizFrame
		var oVizFrame = this.getView().byId("columnChartId");
	//2.Create a JSON Model and set the data 
		var oModel = new sap.ui.model.json.JSONModel();
		var data = {
				book: [{
	                "State": "Maharashtra",
	                "Revenue": 469378.12,
	                "Profit": 173793.31,
	                "Cost": 295584.81,
	                "Units Available": 17336,
	                "Unit Price": 21240.79,
	                "Units Sold": 57571
	            }, {
	                "State": "Tamil Nadu & Pondy",
	                "Revenue": 355940.33,
	                "Profit": 140874.87,
	                "Cost": 215065.45,
	                "Units Available": 11270,
	                "Unit Price": 13139.07,
	                "Units Sold": 48552
	            }, {
	                "State": "Karnataka",
	                "Revenue": 172126.37,
	                "Profit": 56994.34,
	                "Cost": 115132.04,
	                "Units Available": 11248,
	                "Unit Price": 79363.21,
	                "Units Sold": 37303
	            }, {
	                "State": "Kerala",
	                "Revenue": 252835.82,
	                "Profit": 81093.4,
	                "Cost": 171742.42,
	                "Units Available": 14917,
	                "Unit Price": 1143.57,
	                "Units Sold": 51664
	            }, {
	                "State": "Gujarat",
	                "Revenue": 481499.18,
	                "Profit": 150465.23,
	                "Cost": 331033.94,
	                "Units Available": 22449,
	                "Unit Price": 28268.02,
	                "Units Sold": 69005
	            }, {
	            	"State": "Andhra & Telangana",
	                "Revenue": 323097.02,
	                "Profit": 115242.56,
	                "Cost": 207854.46,
	                "Units Available": 17996,
	                "Unit Price": 14566.91,
	                "Units Sold": 45346
	            }, {
	                "State": "Rajasthan",
	                "Revenue": 368528.87,
	                "Profit": 124653.66,
	                "Cost": 243875.22,
	                "Units Available": 23662,
	                "Unit Price": 13469.83,
	                "Units Sold": 66737
	            }
		]};
		oModel.setData(data);
//3. Create Viz dataset to feed to the data to the graph
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			uiConfig : {applicationSet : "fiori"},
			dimensions: [{
                name: 'State',
                value: "{State}"
            }],
            measures: [{name: 'Product1',value: '{Revenue}'},
                       {name: 'Product2',value: '{Profit}'},
                       {name: 'Product3',value: '{Cost}'},
                       {name: 'Product4',value: '{Units Available}'},
                       {name: 'Product5',value: '{Unit Price}'},
                       {name: 'Product6',value: '{Cost}'},
                       {name: 'Product7',value: '{Units Sold}'}
                       ],
            data: {
                path: "/book"
            }
		});
		oVizFrame.setDataset(oDataset);
		oVizFrame.setModel(oModel);
		oVizFrame.setVizType('column');
//4.Set Viz propertie
		var feedPrimaryValues = new sap.viz.ui5.controls.common.feeds.FeedItem({
            'uid': "primaryValues",
            'type': "Measure",
            'values': ["Product1", "Product2", "Product3", "Product4","Product5","Product6","Product7"]
        });
        var feedAxisLabels = new sap.viz.ui5.controls.common.feeds.FeedItem({
            'uid': "axisLabels",
            'type': "Dimension",
            'values': ["State"]
        });
        oVizFrame.setVizProperties({
			legend: {
				title: {
					visible: false
				}
			},
			legendGroup: {
				layout: {
					position: "bottom",
					maxLines: 5	
				}
			},
			title: {
				visible: true,
				text : 'Product wise sales',
				fill: "#d7d7d7"
			},
			valueAxis: {
				title: {
					visible :true,
					text : "Profit"
				}
			},
		})
		 oVizFrame.addFeed(feedPrimaryValues);
        oVizFrame.addFeed(feedAxisLabels);
        
  /////// Donut Chart		
		//1.Get the id of the VizFrame
		var oVizFrame = this.getView().byId("donutChartId");
	//2.Create a JSON Model and set the data
		var oModel = new sap.ui.model.json.JSONModel();
		var data = {
		 'Cars': [
		  		{"State": "Maharashtra","Value": "20"},
		  		{"State": "Tamil Nadu & Pondy","Value": "70"},
		  		{"State": "Karnataka","Value": "60"},
		  		{"State": "Kerala","Value": "80"},
		  		{"State": "Gujarat","Value": "60"},
		  		{"State": "Andhra & Telangana","Value": "50"},
		  		{"State": "Rajasthan","Value": "70"},
		  		]};
				oModel.setData(data);
  //3. Create Viz dataset to feed to the data to the graph
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [{
				name : "State",
				value : "{State}"
					}],
			measures : [{
				name : "Profit (%)",
				value : "{Value}"} ],
			data : {
				path : "/Cars"
			}
		});
		oVizFrame.setDataset(oDataset);
		oVizFrame.setModel(oModel);
		oVizFrame.setVizType('pie');
		
//4.Set Viz properties
		var feedSize = new sap.viz.ui5.controls.common.feeds.FeedItem({
			'uid': "size",
			'type': "Measure",
			'values': ["Profit (%)"]
		}),
		feedColor = new sap.viz.ui5.controls.common.feeds.FeedItem({
			'uid': "color",
			'type': "Dimension",
			'values': ["State"]
		});
		oVizFrame.setVizProperties({
			legend: {
				title: {
					visible: false
				}
			},
			legendGroup: {
				layout: {
					position: "bottom",
					/*maxLines: 5	*/
				}
			},
			title: {
				visible: true,
				text : 'Regional wise sales'
			}
		})
		oVizFrame.addFeed(feedSize);
		oVizFrame.addFeed(feedColor);
		
///// Line Chart
	//1.Get the id of the VizFrame
	var oVizFrame = this.getView().byId("lineChartId");
//2.Create a JSON Model and set the data
	var oModel = new sap.ui.model.json.JSONModel();
	oModel.loadData("json/Dealer.json");
	oModel.setData(data);
//3. Create Viz dataset to feed to the data to the graph
	var oDataset = new sap.viz.ui5.data.FlattenedDataset({
		dimensions : [{
			name : "State",
			value : "{State}"
				}],
		measures: [
		           {name: 'Karthick', value: '{Revenue}'},
		           {name: 'Ramu',value: '{Profit}'},
		           {name: 'Chethan',value: '{Cost}'},
		           {name: 'Nadhan',value: '{Cost}'},
		           {name: 'Abhijeet',value: '{Unit Price}'},
		           {name: 'Raghul',value: '{Units Available}'},
		           {name: 'Prabhu',value: '{Units Sold}'}
		           ],
		data : {
			path : "/book"
		}
	});
	oVizFrame.setDataset(oDataset);
	oVizFrame.setModel(oModel);
	oVizFrame.setVizType('line');
//4.Set Viz properties
	var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
		'uid': "primaryValues",
		'type': "Measure",
		'values': ["Karthick", "Ramu", "Chethan", "Nadhan","Abhijeet","Raghul","Prabhu"]
	}),
	feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
		'uid': "axisLabels",
		'type': "Dimension",
		'values': ["State"]
	});
	oVizFrame.setVizProperties({
		plotArea: {
	 		dataLabel: {
	 			visible: false
	 		}
	 	},
		legend: {
			title: {
				visible: false
			}
		},
		legendGroup: {
			layout: {
				position: "bottom",
				maxWidth : 100,
				alignment : 'center'
			}
		},
		title: {
			visible: true,
			text : 'Dealer wise sales'
		},
		valueAxis: {
			title: {
				visible :true,
				text : "Profit (%)"
			},colorPalette:['red']
		}
	})
	oVizFrame.addFeed(feedValueAxis);
	oVizFrame.addFeed(feedCategoryAxis);
	/*oVizFrame.addFeed(feedColor);*/
	
//PieChart	char state 1
//1.Get the id of the VizFrame
	var oVizFrame = this.getView().byId("pieChartId1");
//2.Create a JSON Model and set the data
	var oModel = new sap.ui.model.json.JSONModel();
	 oModel.loadData("json/Products.json");
//3. Create Viz dataset to feed to the data to the graph
	var oDataset = new sap.viz.ui5.data.FlattenedDataset({
		dimensions : [{
			name : "Product",
			value : "{Product}"
				}],
		
		measures : [{
			name : "Profit",
			value : "{Value}"} ],
		
		data : {
			path : "/Products1"
		}
	});
	oVizFrame.setDataset(oDataset);
	oVizFrame.setModel(oModel);
	oVizFrame.setVizType('pie');
//4.Set Viz properties
	var feedSize = new sap.viz.ui5.controls.common.feeds.FeedItem({
		'uid': "size",
		'type': "Measure",
		'values': ["Profit"]
	}),
	feedColor = new sap.viz.ui5.controls.common.feeds.FeedItem({
		'uid': "color",
		'type': "Dimension",
		'values': ["Product"]
	});
	oVizFrame.setVizProperties({
		legend: {
			title: {
				visible: false
			}
		}, 
		legendGroup: {
			layout: {
				position: "bottom"
			}
		},
		title: {
			visible: true,
			text : 'Pie Chart'
		}
	})
	oVizFrame.addFeed(feedSize);
	oVizFrame.addFeed(feedColor);
	
//PieChart	char state 2
//1.Get the id of the VizFrame
		var oVizFrame = this.getView().byId("pieChartId2");

		var oModel = new sap.ui.model.json.JSONModel();
		 oModel.loadData("json/Products.json");
//3. Create Viz dataset to feed to the data to the graph
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [{name : "Product",value : "{Product}"}],
			measures : [{name : "Profit",value : "{Value}"} ],
			data : {path : "/Products2"}
		});
		oVizFrame.setDataset(oDataset);
		oVizFrame.setModel(oModel);
		oVizFrame.setVizType('pie');
//4.Set Viz properties
	var feedSize = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "size",'type': "Measure",'values': ["Profit"]}),
	feedColor = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "color",'type': "Dimension",'values': ["Product"]});
		oVizFrame.setVizProperties({
			legend: {
				title: {visible: false}
			}, 
			legendGroup: {
				layout: {position: "bottom"}
			},
			title: {visible: true, text : 'Pie Chart'}
		})
		oVizFrame.addFeed(feedSize);
		oVizFrame.addFeed(feedColor);
		
//PieChart	char state 3
//1.Get the id of the VizFrame
		var oVizFrame = this.getView().byId("pieChartId3");

		var oModel = new sap.ui.model.json.JSONModel();
		 oModel.loadData("json/Products.json");
//3. Create Viz dataset to feed to the data to the graph
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [{name : "Product",value : "{Product}"}],
			measures : [{name : "Profit",value : "{Value}"} ],
			data : {path : "/Products3"}
		});
		oVizFrame.setDataset(oDataset);
		oVizFrame.setModel(oModel);
		oVizFrame.setVizType('pie');
//4.Set Viz properties
	var feedSize = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "size",'type': "Measure",'values': ["Profit"]}),
	feedColor = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "color",'type': "Dimension",'values': ["Product"]});
		oVizFrame.setVizProperties({
			legend: {
				title: {visible: false}
			}, 
			legendGroup: {
				layout: {position: "bottom"}
			},
			title: {visible: true, text : 'Pie Chart'}
		})
		oVizFrame.addFeed(feedSize);
		oVizFrame.addFeed(feedColor);
		
//PieChart	char state 4
//1.Get the id of the VizFrame
		var oVizFrame = this.getView().byId("pieChartId4");

		var oModel = new sap.ui.model.json.JSONModel();
		 oModel.loadData("json/Products.json");
//3. Create Viz dataset to feed to the data to the graph
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [{name : "Product",value : "{Product}"}],
			measures : [{name : "Profit",value : "{Value}"} ],
			data : {path : "/Products4"}
		});
		oVizFrame.setDataset(oDataset);
		oVizFrame.setModel(oModel);
		oVizFrame.setVizType('pie');
//4.Set Viz properties
	var feedSize = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "size",'type': "Measure",'values': ["Profit"]}),
	feedColor = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "color",'type': "Dimension",'values': ["Product"]});
		oVizFrame.setVizProperties({
			legend: {
				title: {visible: false}
			}, 
			legendGroup: {
				layout: {position: "bottom"}
			},
			title: {visible: true, text : 'Pie Chart'}
		})
		oVizFrame.addFeed(feedSize);
		oVizFrame.addFeed(feedColor);
	
//PieChart	char state 5
//1.Get the id of the VizFrame
		var oVizFrame = this.getView().byId("pieChartId5");

		var oModel = new sap.ui.model.json.JSONModel();
		 oModel.loadData("json/Products.json");
//3. Create Viz dataset to feed to the data to the graph
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [{name : "Product",value : "{Product}"}],
			measures : [{name : "Profit",value : "{Value}"} ],
			data : {path : "/Products5"}
		});
		oVizFrame.setDataset(oDataset);
		oVizFrame.setModel(oModel);
		oVizFrame.setVizType('pie');
//4.Set Viz properties
	var feedSize = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "size",'type': "Measure",'values': ["Profit"]}),
	feedColor = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "color",'type': "Dimension",'values': ["Product"]});
		oVizFrame.setVizProperties({
			legend: {
				title: {visible: false}
			}, 
			legendGroup: {
				layout: {position: "bottom"}
			},
			title: {visible: true, text : 'Pie Chart'}
		})
		oVizFrame.addFeed(feedSize);
		oVizFrame.addFeed(feedColor);
		
//PieChart	char state 6
//1.Get the id of the VizFrame
		var oVizFrame = this.getView().byId("pieChartId6");

		var oModel = new sap.ui.model.json.JSONModel();
		 oModel.loadData("json/Products.json");
//3. Create Viz dataset to feed to the data to the graph
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [{name : "Product",value : "{Product}"}],
			measures : [{name : "Profit",value : "{Value}"} ],
			data : {path : "/Products6"}
		});
		oVizFrame.setDataset(oDataset);
		oVizFrame.setModel(oModel);
		oVizFrame.setVizType('pie');
//4.Set Viz properties
	var feedSize = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "size",'type': "Measure",'values': ["Profit"]}),
	feedColor = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "color",'type': "Dimension",'values': ["Product"]});
		oVizFrame.setVizProperties({
			legend: {
				title: {visible: false}
			}, 
			legendGroup: {
				layout: {position: "bottom"}
			},
			title: {visible: true, text : 'Pie Chart'}
		})
		oVizFrame.addFeed(feedSize);
		oVizFrame.addFeed(feedColor);
		
//PieChart	char state 7
//1.Get the id of the VizFrame
		var oVizFrame = this.getView().byId("pieChartId7");

		var oModel = new sap.ui.model.json.JSONModel();
		 oModel.loadData("json/Products.json");
	
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [{name : "Product",value : "{Product}"}],
			measures : [{name : "Profit",value : "{Value}"} ],
			data : {path : "/Products7"}
		});
		oVizFrame.setDataset(oDataset);
		oVizFrame.setModel(oModel);
		oVizFrame.setVizType('pie');
//4.Set Viz properties
	var feedSize = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "size",'type': "Measure",'values': ["Profit"]}),
	feedColor = new sap.viz.ui5.controls.common.feeds.FeedItem({'uid': "color",'type': "Dimension",'values': ["Product"]});
		oVizFrame.setVizProperties({
			legend: {
				title: {visible: false}
			}, 
			legendGroup: {
				layout: {position: "bottom"}
			},
			title: {visible: true, text : 'Rajasthan'}
		})
		oVizFrame.addFeed(feedSize);
		oVizFrame.addFeed(feedColor);
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf view.Detail
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf view.Detail
*/
	onAfterRendering: function() {
		sap.ui.getCore().byId("Detail--button1-img").removeStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button1-img").addStyleClass("menu-main");
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf view.Detail
*/
//	onExit: function() {
//
//	}
	handleNavButtonPress: function () {
	    var oSplitApp = this.getView().getParent().getParent();
	    var oMaster = oSplitApp.getMasterPages()[0];
	    oSplitApp.toMaster(oMaster, "fade");
	},
	handlePressConfiguration: function(oEvent) {
		var oItem = oEvent.getSource();
		var oShell = this.getView().byId("myShell");
		var bState = oShell.getShowPane();
		oShell.setShowPane(!bState);
		oItem.setShowMarker(!bState);
		oItem.setSelected(!bState);
	},
	yourMethod: function(oEvent){
	     alert("Custom Button pressed");
	     sap.ui.getCore().byId("splitAppId").to("tableView");
	  },
/////////////  Shelll button..............
	onPressproduct: function(){ 
	//master button active/hover func
		sap.ui.getCore().byId("Detail--button1-img").removeStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button1-img").addStyleClass("menu-main");
		
		sap.ui.getCore().byId("Detail--button0-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button0-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button2-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button2-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button3-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button3-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button4-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button4-img").addStyleClass("sapMBtnIcon");
		
	//Detail page hide/show	
		sap.ui.getCore().byId("Detail--columnChartId").addStyleClass("Show");
		sap.ui.getCore().byId("Detail--columnChartId").removeStyleClass("Hide");
		sap.ui.getCore().byId("Detail--toolbarId").addStyleClass("Show");
		sap.ui.getCore().byId("Detail--toolbarId").removeStyleClass("Hide");
		sap.ui.getCore().byId("Detail--donutChartId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--donutChartId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--lineChartId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--lineChartId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--homePageId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--homePageId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--TablePageId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--TablePageId").removeStyleClass("Show");
	},
	
	onPressregion: function(){
	//master button active/hover func
		sap.ui.getCore().byId("Detail--button2-img").removeStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button2-img").addStyleClass("menu-main");
		
		sap.ui.getCore().byId("Detail--button0-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button0-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button1-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button1-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button3-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button3-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button4-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button4-img").addStyleClass("sapMBtnIcon");
		
	//Detail page hide/show
		sap.ui.getCore().byId("Detail--donutChartId").addStyleClass("Show");
		sap.ui.getCore().byId("Detail--donutChartId").removeStyleClass("Hide");
		sap.ui.getCore().byId("Detail--toolbarId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--toolbarId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--columnChartId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--columnChartId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--lineChartId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--lineChartId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--homePageId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--homePageId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--idIconTabBarMulti").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--idIconTabBarMulti").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--TablePageId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--TablePageId").removeStyleClass("Show");
	},
	onPressdealer: function(){
	//master button active/hover func
		sap.ui.getCore().byId("Detail--button3-img").removeStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button3-img").addStyleClass("menu-main");
		
		sap.ui.getCore().byId("Detail--button0-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button0-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button1-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button1-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button2-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button2-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button4-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button4-img").addStyleClass("sapMBtnIcon");
		
	//Detail page hide/show	
		sap.ui.getCore().byId("Detail--lineChartId").addStyleClass("Show");
		sap.ui.getCore().byId("Detail--lineChartId").removeStyleClass("Hide");
		sap.ui.getCore().byId("Detail--toolbarId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--toolbarId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--columnChartId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--columnChartId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--donutChartId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--donutChartId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--homePageId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--homePageId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--idIconTabBarMulti").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--idIconTabBarMulti").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--TablePageId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--TablePageId").removeStyleClass("Show");
	},
	onPressHome: function(){
	//master button active/hover func
		sap.ui.getCore().byId("Detail--button0-img").removeStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button0-img").addStyleClass("menu-main");
		
		sap.ui.getCore().byId("Detail--button1-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button1-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button2-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button2-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button3-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button3-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button4-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button4-img").addStyleClass("sapMBtnIcon");
		
	//Detail page hide/show
		sap.ui.getCore().byId("Detail--homePageId").addStyleClass("Show");
		sap.ui.getCore().byId("Detail--homePageId").removeStyleClass("Hide");
		sap.ui.getCore().byId("Detail--toolbarId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--toolbarId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--lineChartId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--lineChartId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--columnChartId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--columnChartId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--donutChartId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--donutChartId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--idIconTabBarMulti").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--idIconTabBarMulti").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--TablePageId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--TablePageId").removeStyleClass("Show");
	},
	
	onPressTable: function(){
		sap.ui.getCore().byId("Detail--button4-img").removeStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button4-img").addStyleClass("menu-main");
		
		sap.ui.getCore().byId("Detail--button1-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button1-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button2-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button2-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button3-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button3-img").addStyleClass("sapMBtnIcon");
		sap.ui.getCore().byId("Detail--button0-img").removeStyleClass("menu-main");
		sap.ui.getCore().byId("Detail--button0-img").addStyleClass("sapMBtnIcon");
			
		//Detail page hide/show
			sap.ui.getCore().byId("Detail--TablePageId").addStyleClass("Show");
			sap.ui.getCore().byId("Detail--TablePageId").removeStyleClass("Hide");
			sap.ui.getCore().byId("Detail--homePageId").addStyleClass("Hide");
			sap.ui.getCore().byId("Detail--homePageId").removeStyleClass("Show");
			sap.ui.getCore().byId("Detail--toolbarId").addStyleClass("Hide");
			sap.ui.getCore().byId("Detail--toolbarId").removeStyleClass("Show");
			sap.ui.getCore().byId("Detail--lineChartId").addStyleClass("Hide");
			sap.ui.getCore().byId("Detail--lineChartId").removeStyleClass("Show");
			sap.ui.getCore().byId("Detail--columnChartId").addStyleClass("Hide");
			sap.ui.getCore().byId("Detail--columnChartId").removeStyleClass("Show");
			sap.ui.getCore().byId("Detail--donutChartId").addStyleClass("Hide");
			sap.ui.getCore().byId("Detail--donutChartId").removeStyleClass("Show");
			sap.ui.getCore().byId("Detail--idIconTabBarMulti").addStyleClass("Hide");
			sap.ui.getCore().byId("Detail--idIconTabBarMulti").removeStyleClass("Show");
		},
		
	toolbarBtnPress: function(event){
		sap.ui.getCore().byId("Detail--idIconTabBarMulti").addStyleClass("Show");
		sap.ui.getCore().byId("Detail--idIconTabBarMulti").removeStyleClass("Hide");
		sap.ui.getCore().byId("Detail--columnChartId").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--columnChartId").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--detailBtn2").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--detailBtn2").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--detailBtn1").addStyleClass("Show");
		sap.ui.getCore().byId("Detail--detailBtn1").removeStyleClass("Hide");
		
	},
		backToToolBar: function(){
		sap.ui.getCore().byId("Detail--columnChartId").addStyleClass("Show");
		sap.ui.getCore().byId("Detail--columnChartId").removeStyleClass("Hide");
		sap.ui.getCore().byId("Detail--idIconTabBarMulti").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--idIconTabBarMulti").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--detailBtn1").addStyleClass("Hide");
		sap.ui.getCore().byId("Detail--detailBtn1").removeStyleClass("Show");
		sap.ui.getCore().byId("Detail--detailBtn2").addStyleClass("Show");
		sap.ui.getCore().byId("Detail--detailBtn2").removeStyleClass("Hide");
	},
	
	onPressLogout: function(){
	     sap.ui.getCore().byId("reference").to("DashboardId");
	  },
	  
	 /* onPressTable: function(){
	     sap.ui.getCore().byId("splitAppId").to("tableView");
	  },
	*/

});