sap.ui.controller("vkc.view.Login", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf view.Login
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf view.Login
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf view.Login
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf view.Login
*/
//	onExit: function() {
//
//	}
	onLogin: function(){
		/*sap.ui.getCore().byId("reference").to("splitid");
	}*/
		var text1=this.getView().byId("id_email").getValue();
		var text2=this.getView().byId("id_password").getValue();
		var allfield= true;
		
		if(text1 == "") { 
			allfield=false;
			this.getView().byId("id_email").setValueState("Error");
		    this.getView().byId("id_email").setValueStateText("Email Id is Mandatory");
		}
		else { 
			this.getView().byId("id_email").setValueState("None");
			var text1_regex =/^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;
			if(!text1.match(text1_regex)){
				allfield=false;
			this.getView().byId("id_email").setValueState("Error");
		    this.getView().byId("id_email").setValueStateText("Check the Mail id");
			}
		}
		if(text2 == "") {
			allfield = false;
			this.getView().byId("id_password").setValueState("Error");
		    this.getView().byId("id_password").setValueStateText("Password is Mandatory");
		}
		else { 
			this.getView().byId("id_password").setValueState("None");
			var text2_regex = /^[a-zA-Z0-9]+$/;
			if(!text2.match(text2_regex) || text2.length>10){
					allfield=false;
					this.getView().byId("id_password").setValueState("Error");
				    this.getView().byId("id_password").setValueStateText("Password enter is wrong");
				    }
		    }
		if(allfield == true){
    		/*jQuery.sap.require("sap.m.MessageBox");
    		sap.m.MessageBox.show("Thanks For Sigin",sap.m.MessageBox.Icon.SUCCESS,"success");*/
    		sap.ui.getCore().byId("reference").to("splitid");
    	}else {
    		jQuery.sap.require("sap.m.MessageBox");
    		sap.m.MessageBox.show("Check The Error",sap.m.MessageBox.Icon.ERROR,"error");
    		
    	}
	},
	//liveChange function....
	name: function(){
		
		var text1=this.getView().byId("id_email").getValue();
		
		if(text1 == "" ){
		  this.getView().byId("id_email").setValueState("Error");
		  this.getView().byId("id_email").setValueStateText("Email Id is Mandatory");
	    }
	    else{
		  this.getView().byId("id_email").setValueState("None"); 
	    }
	},
	password: function(){
	  var text2=this.getView().byId("id_password").getValue();
	  if(text2 == "" ){
		  this.getView().byId("id_password").setValueState("Error");
		  this.getView().byId("id_password").setValueStateText("Password is Mandatory");
	}
	else{
		  this.getView().byId("id_password").setValueState("None"); 
	 }
}

});