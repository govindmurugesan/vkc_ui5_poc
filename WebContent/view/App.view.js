sap.ui.jsview("vkc.view.App", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf view.App
	*/ 
	getControllerName : function() {
		return "vkc.view.App";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf view.App
	*/ 
	createContent : function(oController) {
		
		var app = new sap.m.App("reference",{initialPage:"DashboardId"});
		/*var loginpage = sap.ui.view({id:"idloginpage", viewName:"vkc.view.Login", type:sap.ui.core.mvc.ViewType.XML});*/
		var splitapppage = sap.ui.view({id:"splitid", viewName:"vkc.view.SplittApp", type:"JS"});
		var Dashboard = sap.ui.view({id:"DashboardId", viewName:"vkc.view.Dashboard", type:sap.ui.core.mvc.ViewType.XML});
		
		/*app.addPage(loginpage);*/
		app.addPage(splitapppage);
		app.addPage(Dashboard);
		
		return app;
	}

});