sap.ui.jsview("vkc.view.SplittApp", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf view.SplittApp
	*/ 
	getControllerName : function() {
		return "vkc.view.SplittApp";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf view.SplittApp
	*/ 
	createContent : function(oController) {
		var splitApp = new sap.m.SplitApp("splitAppId");
		
		var master = sap.ui.xmlview("Master", "vkc.view.Master");
		var detail = sap.ui.xmlview("Detail", "vkc.view.Detail");
		var tableView = sap.ui.xmlview("tableView", "vkc.view.table");
		
		splitApp.addDetailPage(detail);
		splitApp.addDetailPage(tableView);
		splitApp.addMasterPage(master);
		
		splitApp.setMode("HideMode");
		return splitApp;
	}

});