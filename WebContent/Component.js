jQuery.sap.declare("vkc.Component");
sap.ui.core.UIComponent.extend("vkc.Component", {
	createContent : function() {
		// create root view
		var view = sap.ui.view({
			id : "app",
			viewName : "vkc.view.App",
			type : "JS"
		});
		
		
		var i18nModel = new sap.ui.model.resource.ResourceModel({
			bundleUrl : "i18n/messageBundle.properties"
		});
		view.setModel(i18nModel, "i18nModel");
		var deviceModel = new sap.ui.model.json.JSONModel({
			isTouch: sap.ui.Device.support.touch,
			isNoTouch: !sap.ui.Device.support.touch,
			isPhone: sap.ui.Device.system.phone,
			isNoPhone: !sap.ui.Device.system.phone,
			listMode: sap.ui.Device.system.phone ? "None" : "SingleSelectMaster",
			listItemType: sap.ui.Device.system.phone ? "Active" : "Inactive"
		});
		deviceModel.setDefaultBindingMode("OneWay");
		view.setModel(deviceModel, "device");
		return view;
	}
});